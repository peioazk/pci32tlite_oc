pci32tlite_oc IP core revision history.

Project: pci32tlite_oc
Description: IP PCI Target 32 Bits (WB compatible)

v3.1	2022-01-19	Peio Azkarate
        - Remove previous versions history.
        - Remove maxii_uart project (released as a separate project).
        - Add pci32tlite_oc.ipfpga file.
        - Add pci32tLite_oc_oc_um_dm.odt and update pci32tLite_oc_oc_um_dm.pdf.
v5.1	2022-02-17	Peio Azkarate
        - Update to the latest version tested on HW.
v5.1.1	2022-02-17	Peio Azkarate
        - Update to the latest version tested on HW (update forgotten).
v5.1.2	2022-02-18	Peio Azkarate
        - Add project library pci32tlite in pci32tlite_oc.ipfpga.
