--+-----------------------------------------------------------------+
--| 																|
--|  Copyright (C) 2005-2022 Peio Azkarate, peio.azkarate@gmail.com	|
--| 																|
--|  This source file may be used and distributed without     		|
--|  restriction provided that this copyright statement is not		|
--|  removed from the file and that any derivative work contains	|
--|  the original copyright notice and the associated disclaimer.	|
--|                                                              	|
--|  This source file is free software; you can redistribute it     |
--|  and/or modify it under the terms of the GNU Lesser General     |
--|  Public License as published by the Free Software Foundation;   |
--|  either version 2.1 of the License, or (at your option) any     |
--|  later version.                                                 |
--| 																|
--|  This source is distributed in the hope that it will be         |
--|  useful, but WITHOUT ANY WARRANTY; without even the implied     |
--|  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR        |
--|  PURPOSE.  See the GNU Lesser General Public License for more   |
--|  details.                                                       |
--| 																|
--|  You should have received a copy of the GNU Lesser General      |
--|  Public License along with this source; if not, download it     |
--|  from http://www.opencores.org/lgpl.shtml                       |
--| 																|
--+-----------------------------------------------------------------+
--+--------------------------------------------------------------------------------------------------------------------+
--|  10-12-2021   v0.22     Peio Azkarate
--+--------------------------------------------------------------------------------------------------------------------+
--+-----------------------------------------------------------------------------+
--|							PACKAGE COMPONENTS DECLARATION						|
--+-----------------------------------------------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

package onapackage is
	component sync
	port (
		clk       	: in std_logic;
        d       	: in std_logic;
    	q       	: out std_logic
	);
	end component;

	component synch
	port (
		clk       	: in std_logic;
    	rst       	: in std_logic;
        d       	: in std_logic;
    	q       	: out std_logic
	);
	end component;

	component syncl
	port (
		clk       	: in std_logic;
    	rst       	: in std_logic;
        d       	: in std_logic;
    	q       	: out std_logic
	);
	end component;

	component sync2
	port (
		clk       	: in std_logic;
        d       	: in std_logic;
    	q       	: out std_logic
	);
	end component;

	component sync2h
	port (
		clk       	: in std_logic;
    	rst       	: in std_logic;
        d       	: in std_logic;
    	q       	: out std_logic
	);
	end component;

	component sync2l
	port (
		clk       	: in std_logic;
    	rst       	: in std_logic;
        d       	: in std_logic;
    	q       	: out std_logic
	);
	end component;

	component syncv
	generic ( size: integer := 8 );
	port (
		clk       	: in std_logic;
        d       	: in std_logic_vector(size-1 downto 0);
    	q       	: out std_logic_vector(size-1 downto 0)
	);
	end component;

	component syncv2h
	generic ( size: integer := 8 );
	port (
		clk       	: in std_logic;
    	rst       	: in std_logic;
        d       	: in std_logic_vector(size-1 downto 0);
    	q       	: out std_logic_vector(size-1 downto 0)
	);
	end component;

	component decoder3to8
	port (
        i       	: in std_logic_vector(2 downto 0);
    	o       	: out std_logic_vector(7 downto 0)
	);
	end component;

	component pfs0
	port (
		clk       	: in std_logic;
		rst       	: in std_logic;
        a       	: in std_logic;
    	y       	: out std_logic
	);
	end component;

	component pfb0
	port (
		clk       	: in std_logic;
		rst       	: in std_logic;
        a       	: in std_logic;
    	y       	: out std_logic
	);
	end component;

	component pfs
	port (
		clk       	: in std_logic;
		rst       	: in std_logic;
        a       	: in std_logic;
    	y       	: out std_logic
	);
	end component;

	component pfb
	port (
		clk       	: in std_logic;
		rst       	: in std_logic;
        a       	: in std_logic;
    	y       	: out std_logic
	);
	end component;

	component pfss
	port (
		clk       	: in std_logic;
		rst       	: in std_logic;
        a       	: in std_logic;
    	y       	: out std_logic
	);
	end component;

    component ireg
        generic(SIZE : integer := 8);
    port (
        wb_clk_i    : in std_logic;
        wb_rst_i    : in std_logic;
        wb_dat_o    : out std_logic_vector(SIZE-1 downto 0);
        wb_we_i     : in std_logic;
        wb_stb_i    : in std_logic;
        wb_ack_o    : out std_logic;
        regin_i     : in std_logic_vector(SIZE-1 downto 0)
    );
    end component;

    component oreg
        generic(SIZE : integer := 8);
    port (
        wb_clk_i    : in std_logic;
        wb_rst_i    : in std_logic;
        wb_dat_i    : in std_logic_vector(SIZE-1 downto 0);
        wb_we_i     : in std_logic;
        wb_stb_i    : in std_logic;
        wb_ack_o    : out std_logic;
        regout_o    : out std_logic_vector(SIZE-1 downto 0)
    );
    end component;

    component shf_l
        generic(SIZE : integer := 8);
    port (
        clk_i    : in std_logic;
        rst_i    : in std_logic;
        load_i   : in std_logic;
        se_i     : in std_logic;
        db_i     : in std_logic_vector(SIZE-1 downto 0);
        d_i      : in std_logic;
        qb_o     : out std_logic_vector(SIZE-1 downto 0);
        q_o      : out std_logic
    );
    end component;

	component dfilter
	port (
		clk_i   : in std_logic;
		rst_i   : in std_logic;
		clkEN_i : in std_logic;
		d_i     : in std_logic;
		df_o    : out std_logic
	);
	end component;

	component pfs2w
	port (
		clk    	: in std_logic;
		rst    	: in std_logic;
        a       : in std_logic;
    	y       : out std_logic
	);
	end component;

	component pfb2w
	port (
		clk    	: in std_logic;
		rst    	: in std_logic;
        a       : in std_logic;
    	y       : out std_logic
	);
	end component;

	component pfs3w
	port (
		clk    	: in std_logic;
		rst    	: in std_logic;
        a       : in std_logic;
    	y       : out std_logic
	);
	end component;

	component pfb3w
	port (
		clk    	: in std_logic;
		rst    	: in std_logic;
        a       : in std_logic;
    	y       : out std_logic
	);
	end component;

    component fdiv
	    GENERIC (divide_ratio : integer :=8);
	port(
		clk : in STD_LOGIC;
		rst : in STD_LOGIC;
		q : out STD_LOGIC
	);
    end component;

    component fdiv_prg
        generic (divsize : integer := 8);
	port(
		clk : in STD_LOGIC;
		rst : in STD_LOGIC;
		div : in std_logic_vector(7 downto 0);
		q : out STD_LOGIC
	);
    end component;

    component fdiv_en
        GENERIC (divide_ratio : integer :=8);
	 port(
		 clk : in STD_LOGIC;
		 rst : in STD_LOGIC;
         en : in std_logic;
		 q : out STD_LOGIC
    );
    end component;

    component clkdiv
	    GENERIC (divide_ratio : integer := 2);
	port(
		clk : in STD_LOGIC;
		rst : in STD_LOGIC;
		q   : out STD_LOGIC
	);
    end component;

    component clkdiv_en
	    generic (divide_ratio : integer := 2);
	port(
		clk : in std_logic;
		rst : in std_logic;
		en  : in std_logic;
		q   : out std_logic
	);
    end component;

    component clkdiv_prg
        generic (divsize : integer := 8);
	port(
		clk : in STD_LOGIC;
		rst : in STD_LOGIC;
        div : in std_logic_vector(divsize-1 downto 0);
		q   : out STD_LOGIC
	);
    end component;

	component cnt
	    generic (size : integer :=8);
	port (
		clk  : in std_logic;
		rst  : in std_logic;
        en   : in std_logic;
    	q    : out std_logic_vector(size-1 downto 0)
	);
	end component;

	component cnt_upld
	    generic (SIZE : integer :=8);
	port (
        clk_i  	: in std_logic;
        rst_i  	: in std_logic;
        en_i   	: in std_logic;
        ld_i   	: in std_logic;
        db_i	: in std_logic_vector(size-1 downto 0);
        qb_o   	: out std_logic_vector(size-1 downto 0)
	);
	end component;

    component cnt_upldco
        generic(SIZE : integer := 8);
    port (
        clk_i  	: in std_logic;
        srst_i  : in std_logic;
        en_i   	: in std_logic;
        ld_i   	: in std_logic;
        db_i	: in std_logic_vector(size-1 downto 0);
        qb_o   	: out std_logic_vector(size-1 downto 0);
        co_o    : out std_logic
    );
    end component;

 	component cnt_dwld
	    generic (SIZE : integer :=8);
	port (
        clk_i  	: in std_logic;
        rst_i  	: in std_logic;
        en_i   	: in std_logic;
        ld_i   	: in std_logic;
        db_i	: in std_logic_vector(size-1 downto 0);
        qb_o   	: out std_logic_vector(size-1 downto 0)
	);
	end component;

  	component cnt_dwldco
	    generic (SIZE : integer :=8);
	port (
        clk_i  	: in std_logic;
        rst_i  	: in std_logic;
        en_i   	: in std_logic;
        ld_i   	: in std_logic;
        db_i	: in std_logic_vector(size-1 downto 0);
        qb_o   	: out std_logic_vector(size-1 downto 0);
        co_o   	: out std_logic
	);
	end component;

	component wrack_wb
	    generic (WAITSTATES : integer := 4);
	port (
		clk_i  	: in std_logic;
		rst_i 	: in std_logic;
        stb_i   : in std_logic;
    	wren_o  : out std_logic;
    	ack_o   : out std_logic
	);
	end component;

    component delay_sl
        generic(N : integer := 1);
    port (
        clk_i  	: in std_logic;
        rst_i  	: in std_logic;
        d_i    	: in std_logic;
        q_o    	: out std_logic
    );
    end component;

    component delay_al
        generic(N : integer := 1);
    port (
        clk_i  	: in std_logic;
        rst_i  	: in std_logic;
        d_i    	: in std_logic;
        q_o    	: out std_logic
    );
    end component;

    component delay_sh
        generic(N : integer := 1);
    port (
        clk_i  	: in std_logic;
        rst_i  	: in std_logic;
        d_i    	: in std_logic;
        q_o    	: out std_logic
    );
    end component;

    component delay_ah
        generic(N : integer := 1);
    port (
        clk_i  	: in std_logic;
        rst_i  	: in std_logic;
        d_i    	: in std_logic;
        q_o    	: out std_logic
    );
    end component;

    component ffden
        generic(SIZE : integer := 1);
    port (
        rst_i  	: in std_logic;
        clk_i  	: in std_logic;
        en_i  	: in std_logic;
        d_i    	: in std_logic_vector(SIZE-1 downto 0);
        q_o    	: out std_logic_vector(SIZE-1 downto 0)
    );
    end component;

	component encoder8to3
	port (
        a_i    	: in std_logic_vector(7 downto 0);
    	y_o    	: out std_logic_vector(2 downto 0)
	);
	end component;
end onapackage;

--+-----------------------------------------------------------------------------+
--|								ENTITY & ARCHITECTURE							|
--+-----------------------------------------------------------------------------+
--+-----------------------------------------+
--|  sync									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity sync is
port (
	clk       	: in std_logic;
    d       	: in std_logic;
   	q       	: out std_logic
);
end sync;

architecture rtl of sync is
begin
	SYNCP: process( clk, d )
	begin
        if ( rising_edge(clk) ) then
			q <= d;
		end if;
	end process SYNCP;
end rtl;

--+-----------------------------------------+
--|  synch									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity synch is
port (
	clk       	: in std_logic;
	rst       	: in std_logic;
    d       	: in std_logic;
   	q       	: out std_logic
);
end synch;

architecture rtl of synch is
begin
	SYNCHP: process( clk, rst, d )
	begin
		if (rst = '1') then
			q 	<= '1';
        elsif ( rising_edge(clk) ) then
			q <= d;
		end if;
	end process SYNCHP;
end rtl;

--+-----------------------------------------+
--|  syncl									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity syncl is
port (
	clk       	: in std_logic;
	rst       	: in std_logic;
    d       	: in std_logic;
   	q       	: out std_logic
);
end syncl;

architecture rtl of syncl is
begin
	SYNCLP: process( clk, rst, d )
	begin
		if (rst = '1') then
			q 	<= '0';
        elsif ( rising_edge(clk) ) then
			q <= d;
		end if;
	end process SYNCLP;
end rtl;

--+-----------------------------------------+
--|  sync2									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity sync2 is
	port (
		clk       	: in std_logic;
        d       	: in std_logic;
    	q       	: out std_logic
	);
end sync2;

architecture rtl of sync2 is
	signal tmp:	std_logic;
begin
	SYNC2P: process ( clk, d, tmp)
	begin
        if ( rising_edge(clk) ) then
        	tmp <= d;
        	q <= tmp;
        end if;
	end process SYNC2P;
end rtl;

--+-----------------------------------------+
--|  sync2h									|
--+-----------------------------------------+
-- sync2 con inicializacion a '1' con el reset
library ieee;
use ieee.std_logic_1164.all;

entity sync2h is
	port (
		clk       	: in std_logic;
		rst       	: in std_logic;
        d       	: in std_logic;
    	q       	: out std_logic
	);
end sync2h;

architecture rtl of sync2h is
	signal tmp:	std_logic;
begin
	SYNC2HP: process ( clk, rst, d, tmp)
	begin
		if (rst = '1') then
			tmp <= '1';
			q 	<= '1';
        elsif ( rising_edge(clk) ) then
        	tmp <= d;
        	q <= tmp;
        end if;
	end process SYNC2HP;
end rtl;

--+-----------------------------------------+
--|  sync2l									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity sync2l is
	port (
		clk       	: in std_logic;
		rst       	: in std_logic;
        d       	: in std_logic;
    	q       	: out std_logic
	);
end sync2l;

architecture rtl of sync2l is
	signal tmp:	std_logic;
begin
	SYNC2LP: process ( clk, rst, d, tmp)
	begin
		if (rst = '1') then
			tmp <= '0';
			q 	<= '0';
        elsif ( rising_edge(clk) ) then
        	tmp <= d;
        	q <= tmp;
        end if;
	end process SYNC2LP;
end rtl;

--+-----------------------------------------+
--|  syncv									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity syncv is
generic ( size: integer := 8 );
port (
	clk       	: in std_logic;
    d       	: in std_logic_vector(size-1 downto 0);
   	q       	: out std_logic_vector(size-1 downto 0)
);
end syncv;

architecture rtl of syncv is
begin
	SYNCVP: process( clk, d )
	begin
        if ( rising_edge(clk) ) then
			q <= d;
		end if;
	end process SYNCVP;
end rtl;

--+-----------------------------------------+
--|  syncv2h								|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity syncv2h is
generic ( size: integer := 8 );
port (
	clk       	: in std_logic;
	rst       	: in std_logic;
    d       	: in std_logic_vector(size-1 downto 0);
   	q       	: out std_logic_vector(size-1 downto 0)
);
end syncv2h;

architecture rtl of syncv2h is
	signal tmp:	std_logic_vector(size-1 downto 0);
begin
	SYNCV2HP: process( clk, d, rst )
	begin
		if (rst = '1') then
			tmp <= (others => '1');
			q 	<= (others => '1');
        elsif ( rising_edge(clk) ) then
			tmp <= d;
			q <= tmp;
		end if;
	end process SYNCV2HP;
end rtl;

--+-----------------------------------------+
--|  decoder3to8							|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity decoder3to8 is
port (
    i       	: in std_logic_vector(2 downto 0);
   	o       	: out std_logic_vector(7 downto 0)
);
end decoder3to8;

architecture rtl of decoder3to8 is
begin
	DECOD3TO8P: process( i )
	begin
        if    ( i = "111" ) then o <= "01111111";
		elsif ( i = "110" ) then o <= "10111111";
		elsif ( i = "101" ) then o <= "11011111";
		elsif ( i = "100" ) then o <= "11101111";
		elsif ( i = "011" ) then o <= "11110111";
		elsif ( i = "010" ) then o <= "11111011";
		elsif ( i = "001" ) then o <= "11111101";
		elsif ( i = "000" ) then o <= "11111110";
		else
			o <= "11111111";
		end if;
	end process DECOD3TO8P;
end rtl;

--+-----------------------------------------+
--|  pfs0									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity pfs0 is
port (
	clk       	: in std_logic;
	rst       	: in std_logic;
    a       	: in std_logic;
   	y       	: out std_logic
);
end pfs0;

architecture rtl of pfs0 is
	signal a_s	: std_logic;
begin
	PFSP: process( clk, rst, a )
	begin
		if ( rst = '1' ) then
			a_s  <= '0';
        elsif ( rising_edge(clk) ) then
			a_s  <= a;
		end if;
	end process PFSP;
    --
	y <= a and (not a_s);
end rtl;

--+-----------------------------------------+
--|  pfb0									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity pfb0 is
port (
	clk       	: in std_logic;
	rst       	: in std_logic;
    a       	: in std_logic;
   	y       	: out std_logic
);
end pfb0;

architecture rtl of pfb0 is
	signal a_s	: std_logic;
begin
	PFBP: process( clk, rst, a )
	begin
		if ( rst = '1' ) then
			a_s  <= '0';
        elsif (rising_edge(clk)) then
			a_s  <= a;
		end if;
	end process PFBP;
    --
	y <= (not a) and a_s;
end rtl;

--+-----------------------------------------+
--|  pfs									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity pfs is
port (
	clk       	: in std_logic;
	rst       	: in std_logic;
    a       	: in std_logic;
   	y       	: out std_logic
);
end pfs;

architecture rtl of pfs is
	signal a_s	: std_logic;
	signal a_s2	: std_logic;
begin
	PFSP: process( clk, rst, a )
	begin
		if ( rst = '1' ) then
			a_s  <= '0';
			a_s2 <= '0';
        elsif ( rising_edge(clk) ) then
			a_s  <= a;
			a_s2 <= a_s;
		end if;
	end process PFSP;
    --
	y <= a_s and (not a_s2);
end rtl;

--+-----------------------------------------+
--|  pfb									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity pfb is
port (
	clk       	: in std_logic;
	rst       	: in std_logic;
    a       	: in std_logic;
   	y       	: out std_logic
);
end pfb;

architecture rtl of pfb is
	signal a_s	: std_logic;
	signal a_s2	: std_logic;
begin
	PFBP: process( clk, rst, a )
	begin
		if ( rst = '1' ) then
			a_s  <= '0';
			a_s2 <= '0';
        elsif ( rising_edge(clk) ) then
			a_s  <= a;
			a_s2 <= a_s;
		end if;
	end process PFBP;
    --
	y <= (not a_s) and a_s2;
end rtl;

--+-----------------------------------------+
--|  pfss									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity pfss is
port (
	clk       	: in std_logic;
	rst       	: in std_logic;
    a       	: in std_logic;
   	y       	: out std_logic
);
end pfss;

architecture rtl of pfss is
	signal a_s	: std_logic;
	signal a_s2	: std_logic;
	signal a_s3	: std_logic;
begin
	PFSS_P: process( clk, rst, a )
	begin
		if ( rst = '1' ) then
			a_s  <= '0';
			a_s2 <= '0';
			a_s3 <= '0';
        elsif ( rising_edge(clk) ) then
			a_s  <= a;
			a_s2 <= a_s;
            a_s3 <= a_s2;
		end if;
	end process PFSS_P;
	y <= a_s2 and (not a_s3);
end rtl;

--+-----------------------------------------+
--|  ireg									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;
library onalib;
use onalib.onapackage.all;

entity ireg is
	generic(SIZE : integer := 8);
port (
    wb_clk_i    : in std_logic;
    wb_rst_i    : in std_logic;
    wb_dat_o    : out std_logic_vector(SIZE-1 downto 0);
    wb_we_i     : in std_logic;
    wb_stb_i    : in std_logic;
    wb_ack_o    : out std_logic;
    --
    regin_i     : in std_logic_vector(SIZE-1 downto 0)
);
end ireg;

architecture rtl of ireg is
  	signal rd	: std_logic;
  	signal rd_s	: std_logic;
  	signal ack	: std_logic;
begin
    rd <= '1' when (wb_stb_i = '1' and wb_we_i = '0') else '0';
    uu1: syncl port map ( clk => wb_clk_i, rst => wb_rst_i, d => rd, q => rd_s );
    ack <= '1' when (rd = '1' and rd_s = '1') else '0';
    --
    wb_ack_o <= ack;
	wb_dat_o <= regin_i;
end rtl;

--+-----------------------------------------+
--|  oreg									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;
library onalib;
use onalib.onapackage.all;

entity oreg is
	generic(SIZE : integer := 8);
port (
    wb_clk_i    : in std_logic;
    wb_rst_i    : in std_logic;
    wb_dat_i    : in std_logic_vector(SIZE-1 downto 0);
    wb_we_i     : in std_logic;
    wb_stb_i    : in std_logic;
    wb_ack_o    : out std_logic;
    --
    regout_o    : out std_logic_vector(SIZE-1 downto 0)
);
end oreg;

architecture rtl of oreg is
  	signal wr_en    : std_logic;
  	signal stb_s    : std_logic;
  	signal ack	    : std_logic;
  	signal regout	: std_logic_vector(SIZE-1 downto 0) := (others => '0');
begin
    uu1: syncl port map (clk => wb_clk_i, rst => wb_rst_i, d => wb_stb_i, q => stb_s);
    ack <= '1' when (wb_stb_i = '1' and stb_s = '1') else '0';
    wr_en <= '1' when (ack = '1' and wb_we_i = '1') else '0';

	REGP: process (wb_clk_i, wb_rst_i, wb_dat_i, wr_en)
	begin
		if (wb_rst_i = '1') then
			regout <= (others => '0');
        elsif ( rising_edge(wb_clk_i) ) then
            if (wr_en = '1') then
                regout <= wb_dat_i;
            end if;
		end if;
	end process REGP;
    --
    wb_ack_o <= ack;
	regout_o <= regout;
end rtl;

--+-----------------------------------------+
--|  shf_l									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity shf_l is
	generic(SIZE : integer := 8);
port (
    clk_i    : in std_logic;
    rst_i    : in std_logic;
    load_i   : in std_logic;
    se_i     : in std_logic;
    db_i     : in std_logic_vector(SIZE-1 downto 0);
    d_i      : in std_logic;
    qb_o     : out std_logic_vector(SIZE-1 downto 0);
    q_o      : out std_logic
);
end shf_l;

architecture rtl of shf_l is
  	signal q    : std_logic;
  	signal qb   : std_logic_vector(SIZE-1 downto 0);
begin
	shf_P: process(clk_i, rst_i)
	begin
        if (rst_i = '1') then
            qb <= (others => '0');
            q  <= '0';
        elsif (rising_edge(clk_i)) then
            if (load_i = '1') then
                qb <= db_i;
            elsif (se_i = '1') then
                qb(0) <= d_i;
                qb(SIZE-1 downto 1) <= qb(SIZE-2 downto 0);
                q <= qb(SIZE-1);
            end if;
        end if;
    end process shf_P;
    --
    qb_o <= qb;
    q_o  <= q;
end rtl;


--+-----------------------------------------+
--|  dfilter								|
--+-----------------------------------------+
---------------------------------------------
library ieee ;
use ieee.std_logic_1164.all;

entity dfilter is
port (
    clk_i   : in std_logic;
    rst_i   : in std_logic;
    clkEN_i : in std_logic;
    d_i     : in std_logic;
    df_o    : out std_logic
);
end dfilter;

architecture rtl of dfilter is
    signal df_ffs : std_logic_vector(3 downto 0);
    signal j,k    : std_logic;
    signal qf     : std_logic;
begin
    j <= '1' when (d_i = '1' and df_ffs(3 downto 0) = "1111") else '0';
    k <= '1' when (d_i = '0' and df_ffs(3 downto 0) = "0000") else '0';
	--
    DF_P: process (clk_i, rst_i, clkEN_i, df_ffs, j, k, d_i)
    begin
        if (rst_i = '1') then
            df_ffs <= "0000";
            qf <= '0';
        elsif (clk_i'event and clk_i = '1') then
			if (clkEN_i = '1') then
				if (j = '1') then
					qf <= '1';
				end if;
				if (k = '1') then
					qf <= '0';
				end if;
				df_ffs(3 downto 1) <= df_ffs(2 downto 0);
				df_ffs(0) <= d_i;
			end if;
        end if;
    end process DF_P;
    --
    df_o <= qf;
end rtl;

--+-----------------------------------------+
--|  pfs2w									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity pfs2w is
port (
	clk   : in std_logic;
	rst   : in std_logic;
    a     : in std_logic;
   	y     : out std_logic
);
end pfs2w;

architecture rtl of pfs2w is
	signal a_s	: std_logic;
	signal a_s2	: std_logic;
	signal a_s3	: std_logic;
begin
	PFS2W_P: process(clk, rst, a)
	begin
		if ( rst = '1' ) then
			a_s  <= '0';
			a_s2 <= '0';
			a_s3 <= '0';
        elsif (rising_edge(clk)) then
			a_s  <= a;
			a_s2 <= a_s;
            a_s3 <= a_s2;
		end if;
	end process PFS2W_P;
    --
	y <= a_s and (not a_s3);
end rtl;

--+-----------------------------------------+
--|  pfb2w									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity pfb2w is
port (
	clk       	: in std_logic;
	rst       	: in std_logic;
    a       	: in std_logic;
   	y       	: out std_logic

);
end pfb2w;

architecture rtl of pfb2w is
	signal a_s	: std_logic;
	signal a_s2	: std_logic;
	signal a_s3	: std_logic;
begin
	PFB2WP: process( clk, rst, a )
	begin
		if ( rst = '1' ) then
			a_s  <= '0';
			a_s2 <= '0';
			a_s3 <= '0';
        elsif ( rising_edge(clk) ) then
			a_s  <= a;
			a_s2 <= a_s;
			a_s3 <= a_s2;
		end if;
	end process PFB2WP;
    --
	y <= (not a_s) and a_s3;
end rtl;

--+-----------------------------------------+
--|  pfs3w									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity pfs3w is
port (
	clk   : in std_logic;
	rst   : in std_logic;
    a     : in std_logic;
   	y     : out std_logic
);
end pfs3w;

architecture rtl of pfs3w is
	signal a_s	: std_logic;
	signal a_s2	: std_logic;
	signal a_s3	: std_logic;
	signal a_s4	: std_logic;
begin
	PFS3W_P: process(clk, rst, a)
	begin
		if ( rst = '1' ) then
			a_s  <= '0';
			a_s2 <= '0';
			a_s3 <= '0';
			a_s4 <= '0';
        elsif (rising_edge(clk)) then
			a_s  <= a;
			a_s2 <= a_s;
            a_s3 <= a_s2;
			a_s4 <= a_s3;
		end if;
	end process PFS3W_P;
    --
	y <= a_s and (not a_s4);
end rtl;

--+-----------------------------------------+
--|  pfb3w									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity pfb3w is
port (
	clk       	: in std_logic;
	rst       	: in std_logic;
    a       	: in std_logic;
   	y       	: out std_logic
);
end pfb3w;

architecture rtl of pfb3w is
	signal a_s	: std_logic;
	signal a_s2	: std_logic;
	signal a_s3	: std_logic;
	signal a_s4	: std_logic;
begin
	PFB3WP: process(clk, rst, a)
	begin
		if ( rst = '1' ) then
			a_s  <= '0';
			a_s2 <= '0';
			a_s3 <= '0';
			a_s4 <= '0';
        elsif ( rising_edge(clk) ) then
			a_s  <= a;
			a_s2 <= a_s;
			a_s3 <= a_s2;
			a_s4 <= a_s3;
		end if;
	end process PFB3WP;
    --
	y <= (not a_s) and a_s4;
end rtl;

--+---------------------------------------------------------------------------+
--|  fdiv									                                  |
--+---------------------------------------------------------------------------+
library IEEE;
use IEEE.std_logic_1164.all;

entity fdiv is
	GENERIC (divide_ratio : integer :=8);
	 port(
		 clk : in STD_LOGIC;
		 rst : in STD_LOGIC;
		 q : out STD_LOGIC
	     );
end fdiv;

architecture a of fdiv is
begin
    process(clk,rst)
	    VARIABLE count : INTEGER RANGE 0 TO divide_ratio := 0;
	    VARIABLE q_tmp : std_logic := '0';
    begin --
	    if (rst = '1') then
		    count := 0;
   		    q_tmp := '0';
	    elsif (rising_edge(clk)) then
		    if (count = (divide_ratio -1)) then
			    count := 0;
			    q_tmp := '1';
		    else
			    count := count + 1;
			    q_tmp := '0';
		    end if;
	    end if;
        q <= q_tmp;
    end process;
end a;

--+---------------------------------------------------------------------------+
--|  fdiv_prg							                                      |
--+---------------------------------------------------------------------------+
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity fdiv_prg is
    generic (divsize : integer := 8);
	port(
		 clk : in STD_LOGIC;
		 rst : in STD_LOGIC;
         div : in std_logic_vector(divsize-1 downto 0);
		 q : out STD_LOGIC
	);
end fdiv_prg;

architecture a of fdiv_prg is
begin
    process(clk,rst)
	    VARIABLE count : std_logic_vector(divsize-1 downto 0) := (others => '0');
	    VARIABLE q_tmp : std_logic := '0';
    begin
	    if (rst = '1') then
		    count := div;
   		    q_tmp := '0';
	    elsif (rising_edge(clk)) then
		    if (count = "01") then
			    count := div;
			    q_tmp := '1';
		    else
			    count := count - "01";
			    q_tmp := '0';
		    end if;
	    end if;
        q <= q_tmp;
    end process;
end a;

--+---------------------------------------------------------------------------+
--|  fdiv_en    							                                  |
--+---------------------------------------------------------------------------+
library IEEE;
use IEEE.std_logic_1164.all;

entity fdiv_en is
	GENERIC (divide_ratio : integer :=8);
	 port(
		 clk : in STD_LOGIC;
		 rst : in STD_LOGIC;
         en : in std_logic;
		 q : out STD_LOGIC
	     );
end fdiv_en;

architecture a of fdiv_en is
begin
    process(clk,rst)
	    VARIABLE count : INTEGER RANGE 0 TO divide_ratio := 0;
	    VARIABLE q_tmp : std_logic := '0';
    begin --
	    if (rst = '1') then
		    count := 0;
   		    q_tmp := '0';
	    elsif (rising_edge(clk)) then
            if (en = '1') then
                if (count = (divide_ratio -1)) then
                    count := 0;
                    q_tmp := '1';
                else
                    count := count + 1;
                    q_tmp := '0';
                end if;
            end if;
	    end if;
        q <= q_tmp;
    end process;
end a;

--+---------------------------------------------------------------------------+
--|  clkdiv									                                  |
--+---------------------------------------------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity clkdiv is
	generic (divide_ratio : integer := 2);
	port(
		clk : in STD_LOGIC;
		rst : in STD_LOGIC;
		q : out STD_LOGIC
	);
end clkdiv;

architecture a of clkdiv is
begin
    process(clk,rst)
  	    variable count : integer range 0 TO divide_ratio;
	    variable qtmp : std_logic;
    begin --
	    if (rst = '1') then
		    count := 0;
		    qtmp := '0';
	    elsif (rising_edge(clk)) then
		    if (count = ((divide_ratio/2) -1)) then
			    count := 0;
			    qtmp := not qtmp;
		    else
			    count := count + 1;
			    qtmp := qtmp;
		    end if;
	    end if;
		q <= qtmp;
    end process;
end a;

--+---------------------------------------------------------------------------+
--|  clkdiv_en									                                  |
--+---------------------------------------------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity clkdiv_en is
	generic (divide_ratio : integer := 2);
	port(
		clk : in std_logic;
		rst : in std_logic;
		en : in std_logic;
		q : out std_logic
	);
end clkdiv_en;

architecture a of clkdiv_en is
begin
    process(clk,rst)
  	    variable count : integer range 0 TO divide_ratio;
	    variable qtmp : std_logic;
    begin --
	    if (rst = '1') then
		    count := 0;
		    qtmp := '0';
	    elsif (rising_edge(clk)) then
            if (en = '1') then
                if (count = ((divide_ratio/2) -1)) then
                    count := 0;
                    qtmp := not qtmp;
                else
                    count := count + 1;
                    qtmp := qtmp;
                end if;
            end if;
	    end if;
		q <= qtmp;
    end process;
end a;

--+---------------------------------------------------------------------------+
--|  clkdiv_prg								                                  |
--+---------------------------------------------------------------------------+
library ieee;
use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity clkdiv_prg is
	generic (divsize : integer := 8);
	port(
		clk : in STD_LOGIC;
		rst : in STD_LOGIC;
        div : in std_logic_vector(divsize-1 downto 0);
		q : out STD_LOGIC
	);
end clkdiv_prg;

architecture a of clkdiv_prg is
begin
    CLKDIV_P: process(clk,rst)
	    variable count : std_logic_vector(divsize-1 downto 0) := (others => '0');
	    variable qtmp : std_logic;
    begin --
	    if (rst = '1') then
		    count := '0' & div(divsize-1 downto 1);
		    qtmp := '0';
	    elsif (rising_edge(clk)) then
		    if (count = "01") then
			    count := '0' & div(divsize-1 downto 1);
			    qtmp := not qtmp;
		    else
			    count := count - "01";
			    qtmp := qtmp;
		    end if;
	    end if;
		q <= qtmp;
    end process CLKDIV_P;
end a;

--+-----------------------------------------+
--|  cnt									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity cnt is
	generic(size : integer := 8);
port (
	clk       	: in std_logic;
	rst       	: in std_logic;
    en       	: in std_logic;
   	q       	: out std_logic_vector(size-1 downto 0)
);
end cnt;

architecture rtl of cnt is
begin
	CNT_P: process(clk, rst, en)
        variable iq : std_logic_vector(size-1 downto 0) := (others => '0');
	begin
		if (rst = '1') then
			iq := (others => '0');
        elsif (rising_edge(clk)) then
            if (en = '1') then
                iq := iq + "01";
            end if;
		end if;
        q <= iq;
	end process CNT_P;
end rtl;

--+-----------------------------------------+
--|  cnt_upld								|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity cnt_upld is
	generic(SIZE : integer := 8);
port (
	clk_i  	: in std_logic;
	rst_i  	: in std_logic;
    en_i   	: in std_logic;
    ld_i   	: in std_logic;
    db_i	: in std_logic_vector(size-1 downto 0);
   	qb_o   	: out std_logic_vector(size-1 downto 0)
);
end cnt_upld;

architecture rtl of cnt_upld is
begin
	CNTUPLD_P: process(clk_i, rst_i, en_i, ld_i, db_i)
        variable iq : std_logic_vector(SIZE-1 downto 0) := (others => '0');
	begin
		if (rst_i = '1') then
			iq := (others => '0');
        elsif (rising_edge(clk_i)) then
            if (ld_i = '1') then
                iq := db_i;
            elsif (en_i = '1') then
                iq := iq + "01";
            end if;
		end if;
        qb_o <= iq;
	end process CNTUPLD_P;
end rtl;

--+-----------------------------------------+
--|  cnt_upldco								|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity cnt_upldco is
	generic(SIZE : integer := 8);
port (
	clk_i  	: in std_logic;
	srst_i  : in std_logic;
    en_i   	: in std_logic;
    ld_i   	: in std_logic;
    db_i	: in std_logic_vector(size-1 downto 0);
   	qb_o   	: out std_logic_vector(size-1 downto 0);
    co_o    : out std_logic
);
end cnt_upldco;

architecture rtl of cnt_upldco is
	signal qb	: std_logic_vector(SIZE-1 downto 0);
	signal co	: std_logic;
begin
	CNTUPLDCO_P: process(clk_i)
        variable iq : std_logic_vector(SIZE-1 downto 0) := (others => '0');
	begin
        if (rising_edge(clk_i)) then
            if (srst_i = '1') then
                iq := (others => '0');
            elsif (ld_i = '1') then
                iq := db_i;
            elsif (en_i = '1') then
                iq := iq + "01";
            end if;
		end if;
        qb <= iq;
	end process CNTUPLDCO_P;
    co_o <= '1' when (qb = db_i) else '0';
    qb_o <= qb;
end rtl;

--+-----------------------------------------+
--|  cnt_dwld								|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity cnt_dwld is
	generic(SIZE : integer := 8);
port (
	clk_i  	: in std_logic;
	rst_i  	: in std_logic;
    en_i   	: in std_logic;
    ld_i   	: in std_logic;
    db_i	: in std_logic_vector(size-1 downto 0);
   	qb_o   	: out std_logic_vector(size-1 downto 0)
);
end cnt_dwld;

architecture rtl of cnt_dwld is
begin
	CNTDWLD_P: process(clk_i, rst_i, en_i, ld_i, db_i)
        variable iq : std_logic_vector(SIZE-1 downto 0) := (others => '0');
	begin
		if (rst_i = '1') then
			iq := (others => '0');
        elsif (rising_edge(clk_i)) then
            if (ld_i = '1') then
                iq := db_i;
            elsif (en_i = '1') then
                iq := iq - "01";
            end if;
		end if;
        qb_o <= iq;
	end process CNTDWLD_P;
end rtl;

--+-----------------------------------------+
--|  cnt_dwldco								|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity cnt_dwldco is
	generic(SIZE : integer := 8);
port (
	clk_i  	: in std_logic;
	rst_i  	: in std_logic;
    en_i   	: in std_logic;
    ld_i   	: in std_logic;
    db_i	: in std_logic_vector(size-1 downto 0);
   	qb_o   	: out std_logic_vector(size-1 downto 0);
    co_o   	: out std_logic
);
end cnt_dwldco;

architecture rtl of cnt_dwldco is
	signal qb	: std_logic_vector(SIZE-1 downto 0);
	signal co	: std_logic;
begin
 	CNTDWLD_P: process(clk_i, rst_i, en_i, ld_i, db_i)
        variable iq : std_logic_vector(SIZE-1 downto 0) := (others => '0');
	begin
		if (rst_i = '1') then
			iq := (others => '0');
        elsif (rising_edge(clk_i)) then
            if (ld_i = '1') then
                iq := db_i;
            elsif (en_i = '1') then
                iq := iq - "01";
            end if;
		end if;
        qb <= iq;
	end process CNTDWLD_P;

    -- carry out
	CNTCO_P: process(clk_i, rst_i)
	begin
		if (rst_i = '1') then
			co <= '0';
        elsif (rising_edge(clk_i)) then
            if (ld_i = '1') then
                if (db_i = x"0") then
                    co <= '1';
                else
                    co <= '0';
                end if;
            elsif (en_i = '1') then
                if (qb = x"01") then
                    co <= '1';
                else
                    co <= '0';
                end if;
            end if;
		end if;
	end process CNTCO_P;
    -- outs
    co_o <= co;
    qb_o <= qb;
end rtl;

--+-----------------------------------------+
--|  wrack_wb								|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
library onalib;
use onalib.onapackage.all;

entity wrack_wb is
    generic (WAITSTATES : integer := 4);
port (
	clk_i  	: in std_logic;
	rst_i 	: in std_logic;
    stb_i   : in std_logic;
   	wren_o  : out std_logic;
   	ack_o   : out std_logic
);
end wrack_wb;

architecture rtl of wrack_wb is
	type WRACKFSM is (IDLE, ONWR, ONACK);
  	signal pst_wa   : WRACKFSM;
  	signal nxt_wa   : WRACKFSM;
	signal cntld    : std_logic;
	signal cntld_nx : std_logic;
	signal cnten    : std_logic;
	signal ack      : std_logic;
	signal ack_nx   : std_logic;
	signal wren     : std_logic;
	signal wren_nx  : std_logic;
	signal db	    : std_logic_vector(2 downto 0);
	signal qb	    : std_logic_vector(2 downto 0);
	signal cntq     : integer range 0 to 7;
begin
    db <= conv_std_logic_vector(WAITSTATES-1, 3);
    --
    uu0: component cnt_dwld
        generic map (SIZE => 3)
    port map (
        clk_i => clk_i,
        rst_i => rst_i,
        en_i  => cnten,
        ld_i  => cntld,
        db_i  => db,
        qb_o  => qb
    );
    --
	fsm_ck: process (rst_i, clk_i)
	begin
		if (rst_i = '1') then
			pst_wa <= IDLE;
            cntld  <= '0';
            ack    <= '0';
		elsif (rising_edge(clk_i)) then
			pst_wa <= nxt_wa;
            cntld  <= cntld_nx;
            ack    <= ack_nx;
		end if;
	end process fsm_ck;

	fsm_comb: process (pst_wa, stb_i, cntq)
	begin
        cntld_nx <= '1';
        ack_nx   <= '0';
		case pst_wa is
			when IDLE =>
                if (stb_i = '1') then
                    nxt_wa <= ONWR;
                    cntld_nx <= '0';
                else
                    nxt_wa <= IDLE;
                end if;
			when ONWR =>
                if (cntq = 0) then
                    nxt_wa <= ONACK;
                    ack_nx <= '1';
                else
                    nxt_wa <= ONWR;
                    cntld_nx <= '0';
                end if;
			when ONACK =>
                nxt_wa <= IDLE;
			when OTHERS =>
					nxt_wa <= IDLE;
		end case;
	end process fsm_comb;

    cntq <= conv_integer(qb);
    cnten <= '1' when (pst_wa = ONWR) else '0';
    --
    ack_o  <= ack;
    wren_o <= cnten;

end rtl;

--+-----------------------------------------+
--|  delay_sl								|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity delay_sl is
    generic(N : integer := 1);
port (
	clk_i      	: in std_logic;
	rst_i      	: in std_logic;
    d_i       	: in std_logic;
   	q_o       	: out std_logic
);
end delay_sl;

architecture rtl of delay_sl is
	signal q:	std_logic_vector(N-1 downto 0);
begin
	DEL_P: process (clk_i, rst_i)
	begin
		if (rst_i = '1') then
            for i in 0 to N-1 loop
                q(i) <= '1';
            end loop;
        elsif (rising_edge(clk_i)) then
            if (d_i = '1') then
                for i in 0 to N-1 loop
                    q(i) <= '1';
                end loop;
            else
                q(0) <= d_i;
                for i in 1 to N-1 loop
                    q(i) <= q(i-1);
                end loop;
            end if;
        end if;
	end process DEL_P;
    --
    q_o <= q(N-1);
end rtl;

--+-----------------------------------------+
--|  delay_al								|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity delay_al is
    generic(N : integer := 1);
port (
	clk_i      	: in std_logic;
	rst_i      	: in std_logic;
    d_i       	: in std_logic;
   	q_o       	: out std_logic
);
end delay_al;

architecture rtl of delay_al is
	signal q:	std_logic_vector(N-1 downto 0);
begin
	DEL_P: process (clk_i, rst_i, d_i)
	begin
		if (rst_i = '1' or d_i = '1') then
            for i in 0 to N-1 loop
                q(i) <= '1';
            end loop;
        elsif (rising_edge(clk_i)) then
            q(0) <= '0';
            for i in 1 to N-1 loop
                q(i) <= q(i-1);
            end loop;
        end if;
	end process DEL_P;
    --
    q_o <= q(N-1);
end rtl;

--+-----------------------------------------+
--|  delay_sh								|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity delay_sh is
    generic(N : integer := 1);
port (
	clk_i      	: in std_logic;
	rst_i      	: in std_logic;
    d_i       	: in std_logic;
   	q_o       	: out std_logic
);
end delay_sh;

architecture rtl of delay_sh is
	signal q:	std_logic_vector(N-1 downto 0);
begin
	DEL_P: process (clk_i, rst_i)
	begin
		if (rst_i = '1') then
            for i in 0 to N-1 loop
                q(i) <= '0';
            end loop;
        elsif (rising_edge(clk_i)) then
            if (d_i = '0') then
                for i in 0 to N-1 loop
                    q(i) <= '0';
                end loop;
            else
                q(0) <= d_i;
                for i in 1 to N-1 loop
                    q(i) <= q(i-1);
                end loop;
            end if;
        end if;
	end process DEL_P;
    --
    q_o <= q(N-1);
end rtl;

--+-----------------------------------------+
--|  delay_ah								|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity delay_ah is
    generic(N : integer := 1);
port (
	clk_i      	: in std_logic;
	rst_i      	: in std_logic;
    d_i       	: in std_logic;
   	q_o       	: out std_logic
);
end delay_ah;

architecture rtl of delay_ah is
	signal q:	std_logic_vector(N-1 downto 0);
begin
	DEL_P: process (clk_i, rst_i, d_i)
	begin
		if (rst_i = '1' or d_i = '0') then
            for i in 0 to N-1 loop
                q(i) <= '0';
            end loop;
        elsif (rising_edge(clk_i)) then
            q(0) <= '1';
            for i in 1 to N-1 loop
                q(i) <= q(i-1);
            end loop;
        end if;
	end process DEL_P;
    --
    q_o <= q(N-1);
end rtl;

--+-----------------------------------------+
--|  ffden									|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity ffden is
    generic (SIZE: integer := 1);
port (
    rst_i   : in std_logic;
	clk_i   : in std_logic;
	en_i    : in std_logic;
    d_i    	: in std_logic_vector(SIZE-1 downto 0);
   	q_o    	: out std_logic_vector(SIZE-1 downto 0)
);
end ffden;

architecture rtl of ffden is
begin
	FFDEN_P: process(rst_i, clk_i)
	begin
        if (rst_i = '1') then
            q_o <= (others => '0');
        elsif (rising_edge(clk_i)) then
            if (en_i = '1') then
                q_o <= d_i;
            end if;
		end if;
	end process FFDEN_P;
end rtl;

--+-----------------------------------------+
--|  encoder8to3							|
--+-----------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

entity encoder8to3 is
port (
    a_i    	: in std_logic_vector(7 downto 0);
   	y_o    	: out std_logic_vector(2 downto 0)
);
end encoder8to3;

architecture rtl of encoder8to3 is
begin
    y_o <= "111" when (a_i(7) = '1') else
           "110" when (a_i(6) = '1') else
           "101" when (a_i(5) = '1') else
           "100" when (a_i(4) = '1') else
           "011" when (a_i(3) = '1') else
           "010" when (a_i(2) = '1') else
           "001" when (a_i(1) = '1') else
           "000";
end rtl;
