--+-----------------------------------------------------------------+
--| 																|
--|  Copyright (C) 2005-2022 Peio Azkarate, peio.azkarate@gmail.com	|
--| 																|
--|  This source file may be used and distributed without     		|
--|  restriction provided that this copyright statement is not		|
--|  removed from the file and that any derivative work contains	|
--|  the original copyright notice and the associated disclaimer.	|
--|                                                              	|
--|  This source file is free software; you can redistribute it     |
--|  and/or modify it under the terms of the GNU Lesser General     |
--|  Public License as published by the Free Software Foundation;   |
--|  either version 2.1 of the License, or (at your option) any     |
--|  later version.                                                 |
--| 																|
--|  This source is distributed in the hope that it will be         |
--|  useful, but WITHOUT ANY WARRANTY; without even the implied     |
--|  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR        |
--|  PURPOSE.  See the GNU Lesser General Public License for more   |
--|  details.                                                       |
--| 																|
--|  You should have received a copy of the GNU Lesser General      |
--|  Public License along with this source; if not, download it     |
--|  from http://www.opencores.org/lgpl.shtml                       |
--| 																|
--+-----------------------------------------------------------------+

--+-----------------------------------------------------------------------------+
--|									LIBRARIES									|
--+-----------------------------------------------------------------------------+
library ieee;
use ieee.std_logic_1164.all;

--+-----------------------------------------------------------------------------+
--|									ENTITY   									|
--+-----------------------------------------------------------------------------+
	--+-----------------------------------------+
	--|  PCI32TLITE	IP							|
	--+-----------------------------------------+
entity pci32tlite is
generic (
	vendorID 	 : std_logic_vector(15 downto 0) := x"4150";
	deviceID 	 : std_logic_vector(15 downto 0) := x"0001";
	revisionID 	 : std_logic_vector(7 downto 0)  := x"B2";
	subsystemID  : std_logic_vector(15 downto 0) := x"0000";
   	subsystemvID : std_logic_vector(15 downto 0) := x"1172";
    classcodeID  : std_logic_vector(23 downto 0) := x"068000";
	intpinID 	 : std_logic_vector(7 downto 0)  := x"02";
	-- BAR&WB_CFG (dont delete)
	BARS		: string := "1BARIO";
	WBSIZE		: integer := 8;
	WBENDIAN	: string := "LITTLE"
);
port (
    -- General
    clk33       : in std_logic;
    rst		    : in std_logic;

    -- PCI target 32bits
    ad          : inout std_logic_vector(31 downto 0);
    cbe         : in std_logic_vector(3 downto 0);
    par         : out std_logic;
    frame       : in std_logic;
    irdy        : in std_logic;
    trdy        : out std_logic;
    devsel      : out std_logic;
    stop        : out std_logic;
    idsel       : in std_logic;
    perr        : out std_logic;
    serr        : out std_logic;
    intb        : out std_logic;

	-- Master whisbone
    wb_adr_o     : out std_logic_vector(24 downto 0);
	wb_dat_i     : in std_logic_vector(WBSIZE-1 downto 0);
    wb_dat_o     : out std_logic_vector(WBSIZE-1 downto 0);
	wb_sel_o     : out std_logic_vector(((WBSIZE/8)-1) downto 0);
    wb_we_o      : out std_logic;
	wb_stb_o     : out std_logic;
	wb_cyc_o     : out std_logic;
	wb_ack_i     : in std_logic;
	wb_rty_i     : in std_logic;
	wb_err_i     : in std_logic;
	wb_int_i     : in std_logic
);
end pci32tLite;

--+-----------------------------------------------------------------------------+
--|									ARCHITECTURE								|
--+-----------------------------------------------------------------------------+
architecture rtl of pci32tLite is
--+-----------------------------------------------------------------------------+
--|									COMPONENTS									|
--+-----------------------------------------------------------------------------+
	component pcidec
	generic (
		BARS		 : string := "1BARMEM"
	);
	port (
	    clk_i       	: in std_logic;
   		rst_i       	: in std_logic;
		--
		ad_i			: in std_logic_vector(31 downto 0);
		cbe_i			: in std_logic_vector(3 downto 0);
		idsel_i    	 	: in std_logic;
		bar0_i        	: in std_logic_vector(31 downto 9);
		memEN_i			: in std_logic;
     	ioEN_i			: in std_logic;
		pciadrLD_i	   	: in std_logic;
		adrcfg_o		: out std_logic;
		adrmem_o		: out std_logic;
		adr_o			: out std_logic_vector(24 downto 0);
		cmd_o			: out std_logic_vector(3 downto 0)
	);
	end component;

	component pciwbsequ
	generic (
		BARS		 : string := "1BARMEM";
		WBSIZE		 : integer := 16;
		WBENDIAN	 : string := "BIG"
	);
	port (
   		-- General
	    clk_i       	: in std_logic;
   		rst_i       	: in std_logic;
		-- pci
		cmd_i			: in std_logic_vector(3 downto 0);
		cbe_i			: in std_logic_vector(3 downto 0);
		frame_i    	 	: in std_logic;
		irdy_i        	: in std_logic;
		devsel_o		: out std_logic;
		trdy_o        	: out std_logic;
		stop_o        	: out std_logic;
		-- control
		adrcfg_i		: in std_logic;
		adrmem_i 		: in std_logic;
		pciadrLD_o	   	: out std_logic;
		pcidOE_o		: out std_logic;
		parOE_o			: out std_logic;
		wbdatLD_o   	: out std_logic;
		wrcfg_o 		: out std_logic;
		rdcfg_o 		: out std_logic;
		-- whisbone
		wb_sel_o		: out std_logic_vector(((WBSIZE/8)-1) downto 0);
		wb_we_o			: out std_logic;
		wb_stb_o		: out std_logic;
		wb_cyc_o		: out std_logic;
		wb_ack_i		: in std_logic;
		wb_rty_i    	: in std_logic;
		wb_err_i		: in std_logic
	);
	end component;

	component pcidmux
	generic (
		BARS		 : string := "1BARMEM";
		WBSIZE		 : integer := 16;
		WBENDIAN	 : string := "BIG"
	);
	port (
	    clk_i       	: in std_logic;
   		rst_i       	: in std_logic;
		--
		d_io			: inout std_logic_vector(31 downto 0);
		pcidatout_o		: out std_logic_vector(31 downto 0);
		pcidOE_i		: in std_logic;
		wbdatLD_i		: in std_logic;
 		rdcfg_i			: in std_logic;
		cbe_i			: in std_logic_vector(3 downto 0);
		wb_dat_i		: in std_logic_vector((WBSIZE-1) downto 0);
		wb_dat_o		: out std_logic_vector((WBSIZE-1) downto 0);
		rg_dat_i		: in std_logic_vector(31 downto 0);
		rg_dat_o		: out std_logic_vector(31 downto 0)
	);
	end component;

	component pciregs
	generic (
		vendorID : std_logic_vector(15 downto 0);
		deviceID : std_logic_vector(15 downto 0);
		revisionID : std_logic_vector(7 downto 0);
		subsystemID : std_logic_vector(15 downto 0);
    	subsystemvID : std_logic_vector(15 downto 0);
        classcodeID : std_logic_vector(23 downto 0);
		intpinID : std_logic_vector(7 downto 0);
		BARS		 : string := "1BARMEM"
	);
	port (
	    clk_i       	: in std_logic;
   		rst_i       	: in std_logic;
		--
		adr_i			: in std_logic_vector(7 downto 2);
		cbe_i			: in std_logic_vector(3 downto 0);
		dat_i			: in std_logic_vector(31 downto 0);
		dat_o			: out std_logic_vector(31 downto 0);
   		wrcfg_i       	: in std_logic;
   		rdcfg_i       	: in std_logic;
   		perr_i       	: in std_logic;
   		serr_i       	: in std_logic;
   		tabort_i       	: in std_logic;
		bar0_o			: out std_logic_vector(31 downto 9);
		perrEN_o		: out std_logic;
		serrEN_o		: out std_logic;
		memEN_o			: out std_logic;
	    ioEN_o			: out std_logic
	);
	end component;

	component pcipargen
	port (
		clk_i			: in std_logic;
		pcidatout_i		: in std_logic_vector(31 downto 0);
		cbe_i			: in std_logic_vector(3 downto 0);
		parOE_i 		: in std_logic;
		par_o			: out std_logic
	);
	end component;

--+-----------------------------------------------------------------------------+
--|									CONSTANTS  									|
--+-----------------------------------------------------------------------------+
--+-----------------------------------------------------------------------------+
--|									SIGNALS   									|
--+-----------------------------------------------------------------------------+
	signal bar0			: std_logic_vector(31 downto 9);
	signal memEN		: std_logic;
	signal ioEN			: std_logic;
	signal pciadrLD		: std_logic;
	signal adrcfg		: std_logic;
	signal adrmem		: std_logic;
	signal adr			: std_logic_vector(24 downto 0);
	signal cmd			: std_logic_vector(3 downto 0);
	signal pcidOE		: std_logic;
	signal parOE		: std_logic;
	signal wbdatLD		: std_logic;
	signal wrcfg		: std_logic;
	signal rdcfg		: std_logic;
	signal pcidatread	: std_logic_vector(31 downto 0);
	signal pcidatwrite	: std_logic_vector(31 downto 0);
	signal pcidatout	: std_logic_vector(31 downto 0);
	signal parerr		: std_logic;
	signal syserr		: std_logic;
	signal tabort		: std_logic;
	signal perrEN		: std_logic;
	signal serrEN		: std_logic;

begin
	-- ASSERT
	assert (BARS = "1BARMEM" or BARS = "1BARIO")
         report "ERROR : Bad BAR configuration"
         severity Failure;
	assert ((WBSIZE = 32 and WBENDIAN = "LITTLE") or (WBSIZE = 16) or (WBSIZE = 8 and WBENDIAN = "LITTLE"))
         report "ERROR : Bad WBSIZE/WBENDIAN configuration"
         severity Failure;

    --+-------------------------------------------------------------------------+
    --|  Component instances													|
    --+-------------------------------------------------------------------------+
	--+-----------------------------------------+
	--|  PCI decoder							|
	--+-----------------------------------------+
	u1: component pcidec
	generic map (
		BARS		 => BARS
	)
	port map (
	    clk_i   	=> clk33,
   		rst_i  		=> rst,
		--
		ad_i		=> ad,
		cbe_i		=> cbe,
		idsel_i    	=> idsel,
		bar0_i      => bar0,
		memEN_i		=> memEN,
		ioEN_i		=> ioEN,
		pciadrLD_i	=> pciadrLD,
		adrcfg_o	=> adrcfg,
		adrmem_o	=> adrmem,
		adr_o		=> adr,
		cmd_o		=> cmd
	);

	--+-----------------------------------------+
	--|  PCI-WB Sequencer						|
	--+-----------------------------------------+
	u2: component pciwbsequ
	generic map (
		BARS		 => BARS,
		WBSIZE		 => WBSIZE,
		WBENDIAN	 => WBENDIAN
	)
	port map (
   		-- General
	    clk_i 		=> clk33,
   		rst_i       => rst,
		-- pci
		cmd_i		=> cmd,
		cbe_i		=> cbe,
		frame_i    	=> frame,
		irdy_i      => irdy,
		devsel_o	=> devsel,
		trdy_o      => trdy,
		stop_o      => stop,
		-- control
		adrcfg_i	=> adrcfg,
		adrmem_i 	=> adrmem,
		pciadrLD_o	=> pciadrLD,
		pcidOE_o	=> pcidOE,
		parOE_o		=> parOE,
		wbdatLD_o   => wbdatLD,
		wrcfg_o 	=> wrcfg,
		rdcfg_o 	=> rdcfg,
		-- whisbone
		wb_sel_o	=> wb_sel_o(((WBSIZE/8)-1) downto 0),
		wb_we_o		=> wb_we_o,
		wb_stb_o	=> wb_stb_o,
		wb_cyc_o	=> wb_cyc_o,
		wb_ack_i	=> wb_ack_i,
		wb_rty_i	=> wb_rty_i,
		wb_err_i	=> wb_err_i
	);

	--+-----------------------------------------+
	--|  PCI-wb datamultiplexer					|
	--+-----------------------------------------+
	u3: component pcidmux
	generic map (
		BARS		 => BARS,
		WBSIZE		 => WBSIZE,
		WBENDIAN	 => WBENDIAN
	)
	port map (
	    clk_i   	=> clk33,
   		rst_i  		=> rst,
		--
		d_io		=> ad,
		pcidatout_o	=> pcidatout,
		pcidOE_i	=> pcidOE,
		wbdatLD_i	=> wbdatLD,
        rdcfg_i		=> rdcfg,
		cbe_i		=> cbe,
		wb_dat_i	=> wb_dat_i((WBSIZE-1) downto 0),
		wb_dat_o	=> wb_dat_o((WBSIZE-1) downto 0),
		rg_dat_i	=> pcidatread,
		rg_dat_o	=> pcidatwrite
	);

	--+-----------------------------------------+
	--|  PCI registers							|
	--+-----------------------------------------+
	u4: component pciregs
	generic map (
		vendorID 		=> vendorID,
		deviceID 		=> deviceID,
		revisionID 		=> revisionID,
		subsystemID 	=> subsystemID,
    	subsystemvID 	=> subsystemvID,
    	classcodeID 	=> classcodeID,
		intpinID 		=> intpinID,
		BARS		    => BARS
	)
	port map (
	    clk_i   	=> clk33,
   		rst_i  		=> rst,
		--
		adr_i		=> adr(7 downto 2),
		cbe_i		=> cbe,
		dat_i		=> pcidatwrite,
		dat_o		=> pcidatread,
   		wrcfg_i     => wrcfg,
   		rdcfg_i     => rdcfg,
   		perr_i      => parerr,
   		serr_i      => syserr,
   		tabort_i    => tabort,
		bar0_o		=> bar0,
		perrEN_o	=> perrEN,
		serrEN_o	=> serrEN,
		memEN_o		=> memEN,
     	ioEN_o		=> ioEN
	);

	--+-----------------------------------------+
	--|  PCI Parity Gnerator					|
	--+-----------------------------------------+
	u5: component pcipargen
	port map (
	    clk_i   	=> clk33,
		pcidatout_i	=> pcidatout,
		cbe_i		=> cbe,
		parOE_i		=> parOE,
		par_o		=> par
	);

	--+-----------------------------------------+
	--|  Whisbone Address bus					|
	--+-----------------------------------------+
	wb_adr_o <= adr;

	--+-----------------------------------------+
	--|  unimplemented							|
	--+-----------------------------------------+
	parerr 	<= '0';
	syserr 	<= '0';
	tabort 	<= '0';

	--+-----------------------------------------+
	--|  unused outputs							|
	--+-----------------------------------------+
	perr 	<= 'Z';
	serr	<= '1';

	--+-----------------------------------------+
	--|  Interrupt								|
	--+-----------------------------------------+
	intb	<= not wb_int_i;

end rtl;
